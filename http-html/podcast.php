<?php

	/*		
	**		GMG Heavenbet Podcast Script
	**		Created By Alex
	**		V1.0
	**		28/02/2013
	*/
	
			// Include Brightoncove API File
			//include($_SERVER['DOCUMENT_ROOT'] . '/node/corephp/core-classes/heavenbet/api-brightcove.php');
			//include($_SERVER['DOCUMENT_ROOT'] . '/classes/api-brightcove.php');
			
			require_once('../library/core.php');
			require_once('../library/database.class.php');
				
			// Set header to XML
			header("Content-type: text/xml;");
			
			$db_connect = database_instance::__getInstance();
			$podcast_items = $db_connect->query('SELECT * FROM botb_podcast_items ORDER BY botb_podcast_id DESC');
			
		
			// This Prototype will use the PHP DomDocument To Assemble to podcast RSS feed
			$dom = new DomDocument('1.0', 'utf-8');
			
			// Declare As RSS Document
			$allgames = $dom->createElement('rss');
			$allgames->setAttribute('version', '2.0');
			// Set the rss
			$allgames->setAttribute('xmlns:itunes', 'http://www.itunes.com/dtds/podcast-1.0.dtd');
			$allgames->setAttribute('xmlns:atom', 'http://www.w3.org/2005/Atom' );


			$channel = $dom->createElement('channel');
		
			$channel_title = 					$dom->createElement('title', $config_cms_setup['podcast_title']);
			$channel_link = 					$dom->createElement('link', $config_cms_setup['podcast_link']);
			$channel_lang = 					$dom->createElement('language',$config_cms_setup['podcast_language']);
			$channel_copyright = 				$dom->createElement('copyright',$config_cms_setup['podcast_owner_name']);
			$channel_itunes_subtitle = 			$dom->createElement('itunes:subtitle',$config_cms_setup['podcast_subtitle']);
			$channel_itunes_author =			$dom->createElement('itunes:author',$config_cms_setup['podcast_owner_name']);
			$channel_itunes_summary =			$dom->createElement('itunes:summary',$config_cms_setup['podcast_summary']);
			$channel_description =				$dom->createElement('description',$config_cms_setup['podcast_description']);
			$channel_itunes_owner	 = 			$dom->createElement('itunes:owner');
			
				$channel_itunes_name = $dom->createElement('itunes:name', $config_cms_setup['podcast_owner_name']);
				$channel_itunes_email = $dom->createElement('itunes:email',$config_cms_setup['podcast_owner_email']);
				//	Sub Of Owner
				$channel_itunes_owner->appendChild($channel_itunes_name);
				$channel_itunes_owner->appendChild($channel_itunes_email);
				//	Append To Owner
				
			$channel_itunes_image = $dom->createElement('itunes:image');
				$channel_itunes_image->setAttribute('href', $config_cms_setup['podcast_image']);
			//	Podcast Image
			
			
			$channel_itunes_category	= 	$dom->createElement('itunes:category');
				$channel_itunes_category->setAttribute('text', $config_cms_setup['podcast_categories']);
			//	Podcast Categorys
			$channel_itunes_keywords	=	$dom->createElement('itunes:keywords', $config_cms_setup['podcast_keywords']);
			//	Podcast Keywords
			
			$channel->appendChild($channel_title);
			$channel->appendChild($channel_link);
			$channel->appendChild($channel_lang);
			$channel->appendChild($channel_copyright);
			$channel->appendChild($channel_itunes_subtitle);
			$channel->appendChild($channel_itunes_summary);
			$channel->appendChild($channel_description);
			$channel->appendChild($channel_itunes_owner);
			$channel->appendChild($channel_itunes_image);
			
			$channel->appendChild($channel_itunes_category);
			$channel->appendChild($channel_itunes_keywords);
			
			$allgames->appendChild($channel);


			foreach($podcast_items as $podcast_item):	
		
				
				$item = $dom->createElement('item');	
					
					$item_title				=			$dom->createElement('title', $podcast_item['botb_podcast_title']);
					$item_itunes_author		=			$dom->createElement('itunes:author',"Heavenbet");
					$item_description		=			$dom->createElement('description',"<![CDATA[ {$podcast_item['botb_podcast_title']} ]]>");
					$item_subtitle			=			$dom->createElement('itunes:subtitle', $podcast_item['botb_podcast_description']);
					$item_summary			=			$dom->createElement('itunes:summary', $podcast_item['botb_podcast_description']);
					
						$item_enclosure			=			$dom->createElement('enclosure');
						$item_enclosure->setAttribute('url', $config_cms_setup['podcast_podcast_loc'].$podcast_item['botb_podcast_audio_url']);
						$item_enclosure->setAttribute('type','video/mp3');
						$item_enclosure->setAttribute('length', $podcast_item['botb_podcast_length']);
					
					$item_link				=			$dom->createElement('link',		$config_cms_setup['podcast_link']);
					$item_guid				=			$dom->createElement('guid', 	$config_cms_setup['podcast_podcast_loc'].$podcast_item['botb_podcast_audio_url']);
					$item_pubdate			=			$dom->createElement('pubDate',	$config_cms_setup['botb_podcast_publish_date']);
					$item_category			=			$dom->createElement('category',	$podcast_item['botb_podcast_category']);
					$item_duration			=			$dom->createElement('itunes:duration', $podcast_item['botb_podcast_tidy_length']);
					$item_keywords			=			$dom->createElement('itunes:keywords',	$podcast_item['botb_podcast_tags']);
					
				$item->appendChild($item_title);
				$item->appendChild($item_itunes_author);
				$item->appendChild($item_description);
				$item->appendChild($item_subtitle);
				$item->appendChild($item_summary);
				$item->appendChild($item_enclosure);
				$item->appendChild($item_link);	
				$item->appendChild($item_guid);	
				$item->appendChild($item_pubdate);	
				$item->appendChild($item_category);	
				$item->appendChild($item_duration);			
		
				$channel->appendChild($item);


		
				
			endforeach;	
				

		$dom->appendChild($allgames);
		
		print $dom->saveXML();
		
		
?>