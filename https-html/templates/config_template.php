<?php 

	$isNavConfigActive = true;
	
	$db_connect = database_instance::__getInstance();
	$podcast_items = $db_connect->query("SELECT * FROM podcast_config WHERE podcast_config_id = {$config_cms_setup['podcast_config_id']} ORDER BY podcast_config_id ASC");
	$db_connect->rowCount();
	
	if(!$sessioninit::ses_auth_delete()){
		die("<h1>Your Account Does Not Have Alter/Delete Prvilieges !</h1>");
	}
	
?>

	<style>
	
		.row-pad-config-tem{
			margin-bottom:10px;
		}
		.row-pad-config-tem > .span3{
			border-bottom: 1px solid #CCC;
			font-weight: bold;
			margin-bottom: 15px;
		}

	
	</style>

	<div class="row">
	
		<div class="span12">
			<div class="page-header">
				<img src="<?php echo $config_cms_setup['podcast_image'] ?>" style="height:40px; float:left; margin-right:20px; border-radius:5px;"/>
			  <h1>Config Page <small>Once Its Gone Its Gone</small></h1>
			</div>
		</div>
	
	</div>

	<div class="row row-pad-config-tem">
		<div class="span3">Podcast Show Name</div>
		
		<div class="span7"><input type="text" placeholder="<?php echo $config_cms_setup['podcast_title'] ?>" class="span7" readonly ></div>
		
		<button type="button" class="btn btn-danger span2">Unlock</button>
	</div>
	
	<div class="row row-pad-config-tem">
		<div class="span3">Podcast Link - Uneditable</div>
		<div class="span9"><input type="text" placeholder="<?php echo $config_cms_setup['podcast_podcast_loc'] ?>" class="span9" readonly></div>
	</div>

	<div class="row row-pad-config-tem">
		<div class="span3">Podcast Show Related Link</div>
		<div class="span9"><input type="text" placeholder="<?php echo $config_cms_setup['podcast_link'] ?>" class="span9"></div>
	</div>

	<div class="row row-pad-config-tem">
		<div class="span3">Podcast Default Language</div>
		<div class="span9"><input type="text" placeholder="<?php echo $config_cms_setup['podcast_language'] ?>" class="span9" readonly ></div>
	</div>

	<div class="row row-pad-config-tem">
		<div class="span3">Podcast Subtitle</div>
		<div class="span9"><textarea class="span9" style="height:130px"><?php echo $config_cms_setup['podcast_subtitle'] ?></textarea></div>
	</div>
	
	<div class="row row-pad-config-tem">
		<div class="span3">Podcast Owner Name</div>
		<div class="span9"><input type="text" placeholder="<?php echo $config_cms_setup['podcast_owner_name'] ?>" class="span9"></div>
	</div>	
	
		<div class="row row-pad-config-tem">
		<div class="span3">Podcast Owner Email</div>
		<div class="span9"><input type="text" placeholder="<?php echo $config_cms_setup['podcast_owner_email'] ?>" class="span9"></div>
	</div>	
	
		<div class="row row-pad-config-tem">
		<div class="span3">Podcast Image</div>
		<div class="span9"><input type="text" placeholder="<?php echo $config_cms_setup['podcast_image'] ?>" class="span9"></div>
	</div>	
	
		<div class="row row-pad-config-tem">
		<div class="span3">Podcast Keywords - Comma Seperated</div>
		<div class="span9"><input type="text" placeholder="<?php echo $config_cms_setup['podcast_keywords'] ?>" class="span9"></div>
	</div>	
	
			<div class="row row-pad-config-tem">
		<div class="span3">Podcast Categories - Comma Seperated</div>
		<div class="span9"><input type="text" placeholder="<?php echo $config_cms_setup['podcast_categories'] ?>" class="span9"></div>
	</div>	
	
	<div class="row" style="margin-top:20px;">
		<button type="button" class="btn-large btn-info span12">Ammend Podcast</button>
	</div>
	