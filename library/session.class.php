<?php


class session {
	
	private static $_sessionid;
	private static $_sessionusername = null;
	private static $_sessionremoteip;
	private static $_userlevel;
	private static $session_instance;
	
	private static $_auth_all;
	private static $_auth_wri;
	private static $_auth_read;
	private static $_auth_del;

	

	private static $_auth_level;
	
	private static $tempSalt = "9mYKzmxtM4eNTYCHiwrD4VIZTmwYbQXwJP6tNT0CF_SkESlvzjMGaVmkZVvUYZn36VxU2qMFz0WnCFQ6LIiQxhEgFvj3oRZGvnOD";
	
	protected  function __construct(){
			
		self::startSession();

	}
	
	public static function __getInstance(){
		
		if(!self::$session_instance) self::$session_instance = new session;
		return self::$session_instance;
		
	}

	private static function startSession(){ 
	
		session_start();
	
		self::$_sessionid = session_id();
		
		self::$_sessionremoteip = $_SERVER['REMOTE_ADDR'];
		
		if(isset($_SESSION['username'])){
		
			self::$_sessionusername = $_SESSION['username']; 
			
			$db_connect = database_instance::__getInstance();
			$podcast_items = $db_connect->query("SELECT user_access_level FROM podcast_users WHERE user_name = '{$_SESSION['username']}'");
			
			$podcast_permisions = $db_connect->query("SELECT * FROM podcast_access_levels");
			
			foreach($podcast_permisions as $perm):
			
				if($perm['user_access_level'] == $podcast_items[0]['user_access_level']):
					
					if($perm['user_podcast_all'] == 1)  self::$_auth_all = 100;
					else self::$_auth_all = false;
					
					if($perm['user_access_write'] == 1)  self::$_auth_wri = 100;
					else self::$_auth_all = false;
					
					if($perm['user_access_delete'] == 1)  self::$_auth_del = 100;
					else self::$_auth_all = false;
					
					if($perm['user_access_read'] == 1)  self::$_auth_read = 100;
					else self::$_auth_all = false;
				
				endif;
			
			endforeach;
		
			/**
			echo "YOU HAVE PERMISSION TO - ";
			if(self::$_auth_all) echo "FULL ADMIN PRIVILIDGES - ";
			if(self::$_auth_wri) echo "To Write To Your Podcast - ";
			if(self::$_auth_del) echo "To Delete Local Podcasts - ";
			if(self::$_auth_read) echo "To Read Podcasts - ";
			**/
			
			return true;
			
		}
	
		else {
		
			return false;

		}
	}

	public static function ses_crypto($pass){
	
		$salt = sha1(self::$tempSalt);
		
		$salt = sha1($salt.$pass);
		
		$pass = sha1($pass.$salt);
		
		echo $pass;
		
		return $pass;
		
	}
	
	public static function ses_login($username, $pass){
		
		$pass = self::ses_crypto($pass);
		
		$db_connect = database_instance::__getInstance();
		
		$podcast_items = $db_connect->rowCount("SELECT user_name FROM podcast_users WHERE user_name = '$username' AND user_hash = '$pass'");
		
		if($podcast_items){

			
			self::$_sessionusername == $username;
			
			$_SESSION['loggedin'] = 'Y';
			$_SESSION['username'] = $username;
			
			return true;
			
		 }
		 
		 else{
		 
			return false;
			
		 }
		  		
	}

	public static function ses_get_username(){
		return self::$_sessionusername;
	}
	
	public static function ses_auth_root(){
		return self::$_auth_all;
	}
	
	public static function ses_auth_delete(){
		return self::$_auth_del;
	}
	
	public static function ses_auth_write(){
		return self::$_auth_wri;
	}
	
	public static function ses_auth_read(){
		return self::$_auth_read;
	}
	
	public static function ses_check_login(){
	
		if(empty(self::$_sessionusername)) return false;
		else return true;
	
	}
	
	/***********************
	
	***********************/
	
	public static function ses_logout(){
		
		session_regenerate_id();
		
		foreach ($_SESSION as $var => $val) {
			  $_SESSION[$var] = null;
		  }
		session_unset();
		session_destroy();

         
		
	}
}