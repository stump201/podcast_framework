<?php 


	define('FS' , '/');
	$config_array = 	array('server_root'	=> '/',
							'serverAddress'	=>	'http://85.17.249.230/',
							'imageAddress'	=>	'podcast-img-flags/',
							'document_root'	=>	'/home/nginx-html/',
							'default_tmp_loc'		=>	'https-html/tmp/',
							'default_podcast_loc'		=>	'http-html/podcasts/',
							'default_delete_page'	=>	'https-html/delete/',
							'default_new_page'	=>	'https-html/new/',
							'default_delete_page'	=>	'https-html/delete/',
							'default_header_loc'	=>	'inc/header.php',
							'default_footer_loc'	=>	'inc/footer.php',
							'default_template_loc'	=>	'https-html/templates',
							'default_image_loc'		=>	'http-html/podcast-img-flags/'

						);	